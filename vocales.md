
<html>
<body>
	
	<fieldset method="post" style="width:350px">
		<form action="ContadorVocales.php" method=POST>
			<legend>Texto para contar vocales y consonantes: </legend>
			<textarea name="texto" rows="10" cols="40" style="float:right; width:500px; height:100px;" placeholder="Escribe aquí tu texto" required></textarea>
			<input type="submit" name="enviar" value="Comprobar" style="float:right; margin-top:15px;">
		</form><br>
	
<?php
	if(isset($_POST['enviar'])) {
	
		$texto=$_POST["texto"];
	
$vocales = 0;
$consonantes = 0;

		foreach (count_chars($texto, 1) as $i => $val) 
			{
			if (preg_match('/[aeiouáéíóúü]/i',chr($i)))
					{
					$vocales = $vocales + $val;
					} else if (preg_match('/[a-z]/i',chr($i)))
						{
						 $consonantes= $consonantes + $val;
						}					
			}

	$primera = array(1=>"I", 2=>"II", 3=>"III", 4=>"IV", 5=>"V", 6=>"VI", 7=>"VII", 8=>"VIII", 9=>"IX");
	$diez = array(10=>"X", 11=>"XI", 12=>"XII", 13=>"XII", 14=>"XIV", 15=>"XV", 16=>"XVI", 17=>"XVII", 18=>"XVIII", 19=>"XIX");
	$veinte = array(2=>"XX", 21=>"XXI", 22=>"XXII", 23=>"XXIII", 24=>"XXIV", 25=>"XXV", 26=>"XXVI", 27=>"XXVII", 28=>"XXVIII", 29=>"XXIX");
	$treinta = array(3=>"XXX", 31=>"XXXI", 32=>"XXXII", 33=>"XXXIII", 34=>"XXXIV", 35=>"XXXV", 36=>"XXXVI", 37=>"XXXVII", 38=>"XXXVIII", 39=>"XXXIX");
	$cuarenta = array(4=>"XL", 41=>"XLI", 42=>"XLII", 43=>"XLIII", 44=>"XLIV", 45=>"XLV", 46=>"XLVI", 47=>"XLVII", 48=>"XLVIII", 49=>"XLIX");
	$cincuenta = array(5=>"L", 51=>"LI", 52=>"LII", 53=>"LIII", 54=>"LIV", 55=>"LV", 56=>"LVI", 57=>"LVII", 58=>"LVIII", 59=>"LIX");
	
	if($vocales ==0){
		echo "No hay Vocales";
	}else if($vocales <10){
		echo "Hay ".$primera[$vocales]." Vocales";
	} else if($vocales <20){
		echo "Hay ".$diez[$vocales]." Vocales";
	} else if($vocales <30){
		echo "Hay ".$veinte[$vocales]." Vocales";
	} else if($vocales <40){
		echo "Hay ".$treinta[$vocales]." Vocales";
	} else if($vocales <50){
		echo "Hay ".$cuarenta[$vocales]." Vocales";
	} else if($vocales <60){
		echo "Hay ".$cincuenta[$vocales]." Vocales";
	}else if($vocales >60){
		echo "Hay más de 60 Vocales";
	}
	
	echo "<br>";
	
	if($consonantes == 0){
		echo "No hay Consonantes";
	}else if($consonantes <10){
		echo "Hay ".$primera[$consonantes]." Consonantes";
	} else if($consonantes <20){
		echo "Hay ".$diez[$consonantes]." Consonantes";
	} else if($consonantes <30){
		echo "Hay ".$veinte[$consonantes]." Consonantes";
	} else if($consonantes <40){
		echo "Hay ".$treinta[$consonantes]." Consonantes";
	} else if($consonantes <50){
		echo "Hay ".$cuarenta[$consonantes]." Consonantes";
	} else if($consonantes <60){
		echo "Hay ".$cincuenta[$consonantes]." Consonantes";
	}else if($consonantes >60){
		echo "Hay más de 60 Consonantes";
	}
}
?>

	</fieldset>	
		
</body>
</html>
